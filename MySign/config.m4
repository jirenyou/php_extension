dnl $Id$
dnl config.m4 for extension MySign

dnl Comments in this file start with the string 'dnl'.
dnl Remove where necessary. This file will not work
dnl without editing.

dnl If your extension references something external, use with:

PHP_ARG_WITH(MySign, for MySign support,
[  --with-MySign             Include MySign support])

dnl Otherwise use enable:

PHP_ARG_ENABLE(MySign, whether to enable MySign support,
[  --enable-MySign           Enable MySign support])

if test "$PHP_MYSIGN" != "no"; then
  dnl Write more examples of tests here...

  dnl # --with-MySign -> check with-path
  dnl SEARCH_PATH="/usr/local /usr"     # you might want to change this
  dnl SEARCH_FOR="/include/MySign.h"  # you most likely want to change this
  dnl if test -r $PHP_MYSIGN/$SEARCH_FOR; then # path given as parameter
  dnl   MYSIGN_DIR=$PHP_MYSIGN
  dnl else # search default path list
  dnl   AC_MSG_CHECKING([for MySign files in default path])
  dnl   for i in $SEARCH_PATH ; do
  dnl     if test -r $i/$SEARCH_FOR; then
  dnl       MYSIGN_DIR=$i
  dnl       AC_MSG_RESULT(found in $i)
  dnl     fi
  dnl   done
  dnl fi
  dnl
  dnl if test -z "$MYSIGN_DIR"; then
  dnl   AC_MSG_RESULT([not found])
  dnl   AC_MSG_ERROR([Please reinstall the MySign distribution])
  dnl fi

  dnl # --with-MySign -> add include path
  dnl PHP_ADD_INCLUDE($MYSIGN_DIR/include)

  dnl # --with-MySign -> check for lib and symbol presence
  dnl LIBNAME=MySign # you may want to change this
  dnl LIBSYMBOL=MySign # you most likely want to change this 

  dnl PHP_CHECK_LIBRARY($LIBNAME,$LIBSYMBOL,
  dnl [
  dnl   PHP_ADD_LIBRARY_WITH_PATH($LIBNAME, $MYSIGN_DIR/$PHP_LIBDIR, MYSIGN_SHARED_LIBADD)
  dnl   AC_DEFINE(HAVE_MYSIGNLIB,1,[ ])
  dnl ],[
  dnl   AC_MSG_ERROR([wrong MySign lib version or lib not found])
  dnl ],[
  dnl   -L$MYSIGN_DIR/$PHP_LIBDIR -lm
  dnl ])
  dnl
  dnl PHP_SUBST(MYSIGN_SHARED_LIBADD)

  PHP_NEW_EXTENSION(MySign, MySign.c, $ext_shared,, -DZEND_ENABLE_STATIC_TSRMLS_CACHE=1)
fi
