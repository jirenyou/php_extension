<?php
function dump($data){
    echo "<pre>";
    var_dump($data);
    echo "</pre>";
}

/**
 * demo 扩展
 */

// 扩展默认示例
$result = demo_default("Hello World!");
dump($result);

// 字符串连接
//demo_string_connect();

// 数组值转字符串
$arr = [
    'name'  => 'jirenyou',
    'age'   => 99,
    'sex'   => 0,
    'nick'  => '24K纯帅'
];
$result = demo_array_to_string($arr);
dump($result);

// 数据签名
$arr = [
    'b'     => 1,
    'd'     => 2,
    'a'     => 3,
    'e'     => 4,
    'c'     => 5
];
$result = demo_sign_make($arr);
dump($result);

function my_md5(){
    return md5("a");
}

// 扩展中调用PHP函数
dump("PHP输出：".md5("123"));
$result = "扩展输出：".demo_callback_func("md5","123"); 
dump($result);
