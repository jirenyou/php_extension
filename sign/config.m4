dnl $Id$
dnl config.m4 for extension sign

dnl Comments in this file start with the string 'dnl'.
dnl Remove where necessary. This file will not work
dnl without editing.

dnl If your extension references something external, use with:

PHP_ARG_WITH(sign, for sign support,
[  --with-sign             Include sign support])

dnl Otherwise use enable:

PHP_ARG_ENABLE(sign, whether to enable sign support,
[  --enable-sign           Enable sign support])

if test "$PHP_SIGN" != "no"; then
  dnl Write more examples of tests here...

  dnl # --with-sign -> check with-path
  dnl SEARCH_PATH="/usr/local /usr"     # you might want to change this
  dnl SEARCH_FOR="/include/sign.h"  # you most likely want to change this
  dnl if test -r $PHP_SIGN/$SEARCH_FOR; then # path given as parameter
  dnl   SIGN_DIR=$PHP_SIGN
  dnl else # search default path list
  dnl   AC_MSG_CHECKING([for sign files in default path])
  dnl   for i in $SEARCH_PATH ; do
  dnl     if test -r $i/$SEARCH_FOR; then
  dnl       SIGN_DIR=$i
  dnl       AC_MSG_RESULT(found in $i)
  dnl     fi
  dnl   done
  dnl fi
  dnl
  dnl if test -z "$SIGN_DIR"; then
  dnl   AC_MSG_RESULT([not found])
  dnl   AC_MSG_ERROR([Please reinstall the sign distribution])
  dnl fi

  dnl # --with-sign -> add include path
  dnl PHP_ADD_INCLUDE($SIGN_DIR/include)

  dnl # --with-sign -> check for lib and symbol presence
  dnl LIBNAME=sign # you may want to change this
  dnl LIBSYMBOL=sign # you most likely want to change this 

  dnl PHP_CHECK_LIBRARY($LIBNAME,$LIBSYMBOL,
  dnl [
  dnl   PHP_ADD_LIBRARY_WITH_PATH($LIBNAME, $SIGN_DIR/$PHP_LIBDIR, SIGN_SHARED_LIBADD)
  dnl   AC_DEFINE(HAVE_SIGNLIB,1,[ ])
  dnl ],[
  dnl   AC_MSG_ERROR([wrong sign lib version or lib not found])
  dnl ],[
  dnl   -L$SIGN_DIR/$PHP_LIBDIR -lm
  dnl ])
  dnl
  dnl PHP_SUBST(SIGN_SHARED_LIBADD)

  PHP_NEW_EXTENSION(sign, sign.c, $ext_shared,, -DZEND_ENABLE_STATIC_TSRMLS_CACHE=1)
fi
