/*
  +----------------------------------------------------------------------+
  | PHP Version 7                                                        |
  +----------------------------------------------------------------------+
  | Copyright (c) 1997-2016 The PHP Group                                |
  +----------------------------------------------------------------------+
  | This source file is subject to version 3.01 of the PHP license,      |
  | that is bundled with this package in the file LICENSE, and is        |
  | available through the world-wide-web at the following url:           |
  | http://www.php.net/license/3_01.txt                                  |
  | If you did not receive a copy of the PHP license and are unable to   |
  | obtain it through the world-wide-web, please send a note to          |
  | license@php.net so we can mail you a copy immediately.               |
  +----------------------------------------------------------------------+
  | Author:                                                              |
  +----------------------------------------------------------------------+
*/

/* $Id$ */

#ifndef PHP_DEMO_H
#define PHP_DEMO_H

extern zend_module_entry demo_module_entry;
#define phpext_demo_ptr &demo_module_entry

#define PHP_DEMO_VERSION "0.1.0" /* Replace with version number for your extension */

#ifdef PHP_WIN32
#	define PHP_DEMO_API __declspec(dllexport)
#elif defined(__GNUC__) && __GNUC__ >= 4
#	define PHP_DEMO_API __attribute__ ((visibility("default")))
#else
#	define PHP_DEMO_API
#endif

#ifdef ZTS
#include "TSRM.h"
#endif

/*
  	Declare any global variables you may need between the BEGIN
	and END macros here:

ZEND_BEGIN_MODULE_GLOBALS(demo)
	zend_long  global_value;
	char *global_string;
ZEND_END_MODULE_GLOBALS(demo)
*/

/* Always refer to the globals in your function as DEMO_G(variable).
   You are encouraged to rename these macros something shorter, see
   examples in any other php module directory.
*/
#define DEMO_G(v) ZEND_MODULE_GLOBALS_ACCESSOR(demo, v)

#if defined(ZTS) && defined(COMPILE_DL_DEMO)
ZEND_TSRMLS_CACHE_EXTERN()
#endif

#endif	/* PHP_DEMO_H */


/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * End:
 * vim600: noet sw=4 ts=4 fdm=marker
 * vim<600: noet sw=4 ts=4
 */

        
/*
 * 声明函数
 */
PHP_FUNCTION(demo_default);                         // 扩展默认
PHP_FUNCTION(demo_string_connect);                  // 字符串拼接
PHP_FUNCTION(demo_array_to_string);                 // 一维数组值转字符串   zend_string *返回
PHP_FUNCTION(demo_array_to_string_2);               // 一维数组值转字符串  zval 返回
PHP_FUNCTION(demo_sign_make);                       // 数据签名
PHP_FUNCTION(demo_sign_verify);                     // 数据验签
PHP_FUNCTION(demo_callback_func);                   // 回调PHP函数